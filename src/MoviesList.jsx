import {
  Card, CardImg, CardBody,
  CardTitle, CardSubtitle,
} from 'reactstrap';

const MoviesList = (props) => {
  return (
    <div className="row">
      {props.data.map((data, i) => (
        <div className="col-3" key={i}>
          <Card>
            <CardImg top width="100%" src={data.Poster} alt="Card image cap" />
            <CardBody>
              <CardTitle tag="h5">{data.Title}</CardTitle>
              <CardSubtitle tag="h6" className="mb-2 text-muted">{data.Year}</CardSubtitle>
            </CardBody>
          </Card>
        </div>
      ))}
    </div>
  )
}

export default MoviesList
