// import News from "./News"
// import Movies from "./Movies"
// import Pokemon from "./Pokemon"
import Digimon from './Digimon'

const App = () => {
  return (
    <div className="container">
      {/* <News /> */}
      {/* <Movies /> */}
      {/* <Pokemon /> */}
      <Digimon /> 
    </div>
  );
}

export default App;
