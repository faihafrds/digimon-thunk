import axios from 'axios'

export const types = {
    CHANGE_LOADING: "CHANGE_LOADING",
    SEARCH_DIGIMON_SUCCESS: "SEARCH_DIGIMON_SUCCESS"
}

export const searchDigimon = (name) => (dispatch) => {
    dispatch({type: types.CHANGE_LOADING})
        const url = `https://digimon-api.vercel.app/api/digimon/name/${name}`
        axios.get(url)
            .then(res => {
                console.log(res.data)
                dispatch({type: types.SEARCH_DIGIMON_SUCCESS, digimon: res.data[0].img})
            })
            .catch(err => console.log(err))
}