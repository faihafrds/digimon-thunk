import { types } from './action'

const globalState = {
    digimon: "",
    isLoading: false
}

const rootReducer = (state=globalState, action) => {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return{
                ...state,
                isLoading: true
            }
        case types.SEARCH_DIGIMON_SUCCESS:
            return{
                ...state,
                digimon: action.digimon,
                isLoading: false
            }
        default:
            return state
    }
}

export default rootReducer