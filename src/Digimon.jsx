import { useState } from 'react'
import { Button, Form, FormGroup, Input, Label, Spinner} from "reactstrap";
import { useSelector, useDispatch } from 'react-redux'
import { searchDigimon } from './redux/action'


const Digimon = () => {

    const [name, setName ] = useState("")
    const globalState = useSelector(state => state)
    console.log(globalState);
    const digimon = useSelector(state => state.digimon)
    const dispatch = useDispatch()


    const handleName = (e) => {
        setName(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(searchDigimon(name))
    }
    return (
        <div>
            <div className="row">
            <div className="col-12">
            <h2>Digimon</h2>
            <Form inline onSubmit={handleSubmit}>
                <FormGroup>
                <Label className="mr-2">Name</Label>
                <Input className="mr-2" type="text" name="name" placeholder="Digimon Name" onChange={handleName} />
                </FormGroup>
                <Button type="submit">Search</Button>
            </Form>
            {globalState.isLoading && <Spinner color="primary" />}
            {!globalState.isLoading && digimon && <img src={digimon} alt="digimon"/>}
            </div>
        </div>
        </div>
    )
}

export default Digimon
