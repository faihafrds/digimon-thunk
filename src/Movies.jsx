import { Component } from "react"
import { Button, Form, FormGroup, Input, Label, Spinner } from "reactstrap";
import MoviesList from "./MoviesList"

class Movies extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      movies: null,
      isLoading: false,
    }
  }

  handleTitle = (e) => {
    this.setState({
      title: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    this.setState({ isLoading: true })

    const params = new URLSearchParams({
      apikey: "6d1f3e3e",
      s: this.state.title
    })
    const url = `https://www.omdbapi.com/?${params}`

    fetch(url)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setTimeout(() => {
          this.setState({
            movies: data.Search,
            isLoading: false
          })  
        }, 1000);
      })
      .catch(err => console.log(err))
  }
  
  render() {
    return (
      <div className="row">
        <div className="col-12">
          <h2>Movies</h2>
          <Form inline onSubmit={this.handleSubmit}>
            <FormGroup>
              <Label className="mr-2">Title</Label>
              <Input className="mr-2" type="text" name="title" placeholder="Your Movie Title" onChange={this.handleTitle} />
            </FormGroup>
            <Button type="submit">Search</Button>
          </Form>
          <hr />
          {this.state.isLoading && <Spinner color="danger" />}
          {this.state.movies && !this.state.isLoading && <MoviesList data={this.state.movies} />}
        </div>
      </div>
    )
  }
}

export default Movies;