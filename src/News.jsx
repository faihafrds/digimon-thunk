import { Component } from "react"

class News extends Component {
  constructor() {
    super();
    this.state = {
      data: null,
      isLoading: true,
    }
  }

  componentDidMount() {
    console.log("[News.jsx] - Berhasil Mount!");
    setTimeout(() => {
      this.getData()  
    }, 2000);
  }
  

  getData = () => {
    const url = "https://berita-indo-api.vercel.app/v1/cnbc-news/"
    fetch(url)
      .then(response => response.json())
      .then(res => {
        this.setState({
          data: res.data,
          isLoading: false,
        })
      })
      .catch(err => console.log(err))
  }

  render() {
    return (
      <>
        <h2>News</h2>
        <div className="news-list">
          <ul>
            {this.state.isLoading && <p>Loading......</p>}
            {this.state.data && !this.state.isLoading &&
              this.state.data.map((news, i) => <li key={i}>{news.title}</li>)}
          </ul>
        </div>
      </>
    )
  }
}

export default News;