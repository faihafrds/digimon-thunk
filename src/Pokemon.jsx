import { Component } from "react"
import { Button, Form, FormGroup, Input, Label, Spinner } from "reactstrap";

class Pokemon extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      pokemon: null,
      name: "",
    }
  }

  handleName = (e) => {
    this.setState({ name: e.target.value})
  }

  handleSubmit = (e) => {
    e.preventDefault()
    const url = `https://pokeapi.co/api/v2/pokemon/${this.state.name}`
    fetch(url)
      .then(res => res.json())
      .then(data => {
        const urlPokemon = data.forms[0].url;
        fetch(urlPokemon)
          .then(res => res.json())
          .then(data => console.log(data.sprites))
          .catch(err => console.log(err))
      })
      .catch(err => console.log(err))
  }

  render() {
    return (
      <div className="row">
        <div className="col-12">
          <h2>Pokemon</h2>
          <Form inline onSubmit={this.handleSubmit}>
            <FormGroup>
              <Label className="mr-2">Name</Label>
              <Input className="mr-2" type="text" name="name" placeholder="Pokemon Name" onChange={this.handleName} />
            </FormGroup>
            <Button type="submit">Search</Button>
          </Form>
        </div>
      </div>
      
    )
  }
}

export default Pokemon;